**PlotID**

`PlotID` is a toolkit that labels your plots and figures and copies all associated data (research data, plotting script, user-functions and a copy of the figure) to the desired location.\
This version of  `PlotID` is build for `MATLAB`. \
`PlotID` provides two core functions **TagPlot** and **Publish** which are described in detail below.

Feel free to give feedback and feature requests or to take part in the development process.

 [TOC]

# Quick User Guide
**Requirements:** MATLAB R2021a or newer

**First use:**
1. Put your PlotID folder in you [userpath](https://uk.mathworks.com/help/matlab/ref/userpath.html) on Windows this is
`C:\Users\USERNAME\Documents\MATLAB`. \
When using a diffrent folder, make sure the folder is on your [search path](https://uk.mathworks.com/help/matlab/matlab_env/add-remove-or-reorder-folders-on-the-search-path.html).

2. Run `Initialisation.m` and follow the command line instructions.\
The **ProjectID** is your custom project number, it well be the prefix of the ID created by `PlotID` (e.g. JL-001).  Save your config file.

**Intended use:**\
`PlotID` works in two Steps:

1. Tag the plot
`[figs, IDs] = PlotID.TagPlot(figs, options)`

2. Publish the plot and the associated data
`PlotID.Publish(DataPaths,scriptPath,  figure, options)`

`scriptPath` contains either the path to the script, this can be provided with the simple call of `scriptPath = [mfilename('fullpath'),'.m']` or the script or function name that is used for creating your plot. \
`DataPaths` contains the path(s) to the research data, for multiple files you can use a cell array (We recommend using absolute paths). It is also possible to pass an arbitrary number of variables as struct. To generate all paths for a filetype try: `f=dir(/folderpath/*.mat); rdata=fullfile({f.folder},{f.name})`. \
`figure` is the figure that should be published.

**Minimal Example:**
```matlab
fig = figure;
plot(x,y);
[fig, IDs] = PlotID.TagPlot(fig);
script = [mfilename('fullpath'),'.m']; % This creates a path to the script this line is  called from
rdata =  {'\\fullpath\dataset01.mat', '\\fullpath\dataset02.h5'} ; % a single path does not need a cell array
PlotID.Publish(rdata, script, fig);
```


**For further insight, please take a look at the examples provided in the Example folder**.


# PlotID.TagPlot()
`[figs, IDs] = TagPlot(figs, options)`
**TagPlot** adds IDs to figures
The ID is placed visual on the figure window and as Tag (property of the figure)
**TagPlot** can tag multiple figures at once. If a single Plot is tagged, IDs is a char, otherwise it is a cell array of chars .

<details><summary>detailed description</summary>

_options_ \
you find the options for TagPlot below. The data type is in curled braces and the default value follows the equal sign.

- ProjectID  {mustBeText}= ''
- Fontsize {mustBeInteger} = 8
- Color  {mustBeNonnegative} = 0.65*[1 1 1]
- Location  {mustBeText} = 'east'
- Position  {mustBeVector} = [1,0.4]
- DistanceToAxis {mustBeReal} = .02
- Rotation  {mustBeInteger} = 0
- PinToLegend {mustBeNumericOrLogical} = false
- QRcode  {mustBeNumericOrLogical} = false
- QRsize {mustBeNonnegative} = 0.15

`options.ProjectID` is the project number (string or char), if empty the ID from the config file is used
The ID is placed on the 'east' of the figure per default, if you want it somwhere else, use the `'Location'` option. 'north', 'east', 'south', 'west' are predefined, otherwise use the `'custom'` option and provide the desired 'Position'  as a vector relative to your x- and y-axis limits `[relX, relY]` .

`options.PinToLegend` pins the ID to the figure's legend. If no legend was defined, plotID will create one.

</details>

## CreateID()
`function [ID] = CreateID(method)` \
CreateID Creates an identifier (char). It creates an (sometimes unique) identifier based on the selected method, if no method is selected method 1 will be the default method.

1. **UNIX Time in seconds as HEX** \
This is used by default due to its simplicity and it is human readable.
2. **random UUID from Java 128 bit.**\
Static factory to retrieve a type 4 (pseudo randomly generated) UUID. The UUID is generated using a cryptographically strong pseudo random number generator.

## friendlyID()
`[IDf,PrjID, Str] = friendlyID(ID)` \
FriendlyID Changes the Hex Number to a human friendly *datetime* and *dateStr*. `IDf` returns the ID as DateTime Object, `PrjID` returns the ProjectID, `Str` returns the timestamp as String. \
**This only works with ID method 1 (UNIX Time as Hex).**


# PlotID.Publish()
`Publish(DataPaths,scriptPath, figure, options)` \
Publishes saves plot, data and measuring script
Location sets the storage location. 'local' sets the storage location to the current folder (an export folder will be created), 'manual' will allow you to choose a folder via user dialogue. If you defined an export path in the config file, this will used per default (exportPath).

Two Methods are implemented: 'individual' stores the data for each plot with the plot while 'centralized' uses a single data folder and creates reference links to the original data (hdf5 only). ParentFolder is the folder name where the exported data is stored if a path is used, PlotId will use this path as storagePath.

<details><summary>detailed description</summary>

_options_ \
You can find the possible options for publish below. The the possible parameters are listed in curled braces and the default value follows the equal sign at the end.

- Location {mustBeMember(options.Location ,{'local','server','manual','CI-Test'})} = 'local' \
'local' creates the folder in your current working path, 'server' will use a remote path that needs to be specified in the config file. 'manual' opens an UI window where the path must be selected, 'CI-Test' is for CI testing only.
- Method {mustBeMember(options.Method ,{'individual','centraliced'})} = 'individual'\
'individual' each folder contains the assigned data files. (recommended for small file sizes and for sharing fully functional packages).
'centralized' one central data folder is used for saving the research data files, the subfolders contain linked hdf5-files (if hdf5 is used). This is recommended if multiple plots are made from the same data set.  Attention, the linked HDF5 will not work when a subfolder was moved or the data folder was deleted.
- ParentFolder  {mustBeText} = 'export'\
Adjust the default parent folder name for exports.
- CopyUserFCN  {mustBeNumericOrLogical} = true \
All user functions that are used by script that calls publish will be exported per default as well. Set this to false (not recommended) to prevent this.
- CSV {mustBeNumericOrLogical} = false \
An overview of all published plots by ID and their associated data will be stored in a .csv file.
</details>

## createFileCopy ()
`[] = createFileCopy(filePaths,folderName,storPath,ID,type)`\
Creates a copy of  files (can be used for multiple paths in a cell array). folderName is the name of the exporting folder
## createLinkedHDF ()
`[status] = createLinkedHDF5(SourceFile,TargetPath,ID)` \
createLinkedHDF5 creates an HDF file that references the source file. TargetPath describes the storage location, ID the foldername, status returns 'true' if the function was successfull.
## fileCompare()
`[status, id] = fileCompare(filename,fileList)`  \
fileCompare checks if file1 is (binary) identical to a file in filelist, it returns a status and the id of the identical file. The function uses the windows system function fc or the unix function diff.
## removePltIdFiles()
`[fListClean] = removePltIdFiles(fList)` \
removePltIdFiles removes functions that are part of PlotID out of flist.

# How to use the .config file
The config file is a JSON-File. The config file stores user settings and options of `PlotID`. It will be created when you use PlotID for the first time. Follow the Instructions to create the file. _Note:_ for basic usage no manual changes on the config file are required. \

**Note:** If you mess up your config-file simply delete it, PlotID will prompt you to create a new one the next time you use it.

## Default config.json


## Define an export path
If you define an export path in the config.json this path is used as location for the exported data.

## Example config.json

```
{
  "Author": "Name",
  "ProjectID": "XYZ01",
  "ExportPath": "C:\\project\\plot_ID"
}
```

## Store custom options for Publish
**add custom publish options to the config file** \
you can define your custom options for publish in the config file. **Important:** They will always have priority against Name-Value pairs!


```
configFileName = 'config.json';
configObj = PlotID.config(configFileName);
```
This method adds the default config to your config file
```
configObj.addPublishOptions('default');
```

You can change the options in the json file, but be careful no argumentValidation will be done on options from the config file!

## Working with multiple (project dependend) config files

<details><summary> Expand </summary>
It is possible to use individual config files for each project (configP1.json, configP2.json, ...) \

1. Create a new config file programatically
```
customConfigFileName = 'configP1.json';
configObj = PlotID.config(customConfigFileName);
configObj.addPublishOptions('default');
```

2. Use the custom Config
you need to provide the name of your custom config file to tagplot as well as to publish by using the Name-Value Pair:
` 'ConfigFileName','YOURNAME.json'` :

```
 [figs, IDs] = PlotID.TagPlot(figs, 'ConfigFileName','configP1.json');
 Publish(DataPaths,scriptPath, 'ConfigFileName','configP1.json')
```

</details>

# Aknowledgements
The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.

# Known Issues
**Closed figure will cause error** Please do not cloase the figure, MATLAB deletes the object if you do so. \
**Centralized Method does not work on Linux systems**

