%% Example Script
% This Script is mend to demonstrate the capabilities of the PlotID tool.
% please run Initilisation.m before first time use

%% Clear and set environment
addpath('Examples');
clear; close all; clc;

%% Data (only necessary for this example)
[x, y, datapath] = createExampleData('matlab');

%% Plotting
% This is still part of a normal script to produce plots.
% Make sure to save each figure in a variable to pass it to PlotID-functions.
fig1 = figure;
plot(x,y,'-k'); box off; set(gca, 'TickDir', 'out', 'YLim', [0,4]);

% -------------- PlotID Implementation starts here. -------------------------

%% ----- 1. Tag the plot -----
% TagPlot adds visible ID(s) to the figure(s) and to the figures property 'Tag'
% every time you run tagPlot you will get an new ID

% Syntax: 
% fig = PlotID.TagPlot(fig);
% [figs, IDs] = PlotID.TagPlot(figs);
% [figs, IDs] = PlotID.TagPlot(figs, options);

[fig1, ID] = PlotID.TagPlot(fig1);

% you can influence the design and position of the Tag with the following
% Name-Value Arguments: 'Fontsize', 'Color', 'Location',
% 'DistanceToAxis','Position','Rotation' 

fig2 = figure; plot(x,y,'-k'); box off; set(gca, 'TickDir', 'out', 'YLim', [0,4]);

% Example: blue tag with fontsize 12 in the south
[fig2, ID] = PlotID.TagPlot(fig2, 'FontSize', 12, 'Color', [0 0 1],...
    'Location','south');

% You can find the description to all options in the readme

%%  ----  2. Publishing -----
% Second part of PlotID 
% Exports your plot, the research data and the associated scripts to a
% folder named with your ID

% The functions needs the file location of the script, the location of the
% data and the figure and can take several options (see readme). 

% Syntax: 
% Publish(DataPaths,scriptPath, figure)
% Publish(DataPaths,scriptPath, figure, options)

% Path of the m.script that you use for creating your plot.
% note: mfilename only works if called from within a script or function
scriptPath = [mfilename('fullpath'),'.m']; 

% file paths of the datasets used for the plot (don't forget the extension) 
% note: use absolute paths for best practice
% datapath = 'test_data.mat'; 
locations =  datapath; 

%call publish
PlotID.Publish(locations,scriptPath, fig2)

% Your plot, script and all the data that you provided are now published.

% ---------------------------------
%% Further examples and help
% You find more examples in the Examples folder:
%   - Passing Variables
%   - Advanced usage 
%   - Working with HDF5-files
%   - Using a central data folder
%   - How to use advanced config-files