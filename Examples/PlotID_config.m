%% Working with one config file

%% add custom publish options to the config file
% you can define custom options in the config file:

configFileName = 'config.json';
configObj = PlotID.config(configFileName);

% This method adds the default config to your config file
configObj.addPublishOptions('default');
% You can change this in the json file, but be carefull no
% argumentValidation will be done on options from the config file!

%% Working with multiple (project dependend) config files
% it is possible to use individual config files for each project
% configP1.json, configP2.json ... 

% Create a new config file programatically 
customConfigFileName = 'configP1.json';
configObj = PlotID.config(customConfigFileName);
configObj.addPublishOptions('default');

% Use the custom Config
% you need to provide the name of your custom config file to tagplot as
% well as to publish by using the Name-Value Pair:
% 'ConfigFileName','YOURNAME.json'

% [figs, IDs] = PlotID.TagPlot(figs, 'ConfigFileName','configP1.json');
% Publish(DataPaths,scriptPath, 'ConfigFileName','configP1.json')
