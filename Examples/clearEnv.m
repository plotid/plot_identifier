function [] = clearEnv()
%CLEARENV clears Environment with exception for CI testing.

if ~exist('ciTest', 'var') 
    clear; close all; clc;
end

end

