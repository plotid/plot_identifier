clearEnv;
%% Example Script - How to pass Variables
% This script how to pass variables instead or additional to the DataPaths
addpath('Examples');
%% Data (only necessary for this example)
[x, y, datapath] = createExampleData('matlab');
scriptPath = mfilename('fullpath');

%% Plotting
% This is still part of a normal script to produce plots.
% Make sure to save each figure in a variable to pass it to PlotID-functions.
fig1 = figure;
plot(x,y,'-k'); box off; set(gca, 'TickDir', 'out', 'YLim', [0,4]);
[fig1, ID] = PlotID.TagPlot(fig1);

%%  ----  2. Publishing -----
% You can pass an abitrary number of variables to Publish.
% Passing multiple structs will create multiple data files.
% You can additionally add datapaths and combine the methods.


%Example: Passing the variables x,y to publish

s.x =x; s.y=y; % Save both variables in struct s

% Build the locations cell with the struct and one path or an array of paths
locations =  {s,datapath}; 

%call publish
PlotID.Publish(locations,scriptPath, fig1)

% Your plot, script, the variables x,y and the path that your provided
% are now published.
