clearEnv;
%%  multiple plots from the same data-set (centralized method)
% A central data folder is used for saving the research data files, the
% subfolders contain linked hdf5-files (if hdf5 is used) otherwise the data
% will only be in the data folder.

% This is recommended, if many plots are made from the same data set.
% Attention, the linked HDF5 will not work when a subfolder was moved or the data
% folder was deleted

[x, y, datapath] = createExampleData('hdf');
scriptPath = [mfilename('fullpath'),'.m'];

fig1 = figure;
plot(x,y,'-k'); 
[fig1, ID] = PlotID.TagPlot(fig1);

PlotID.Publish(datapath,scriptPath, fig1, 'Method','centralized')

% Second figure based on the same data set as fig1
fig2 = figure;
plot(x.^2,y,'-r'); 
[fig2, ID] = PlotID.TagPlot(fig2);

PlotID.Publish(datapath,scriptPath, fig2, 'Method','centralized')

%% Note:
% If you rerun this script PlotId will tell you that an identically named (but
% not binary identical) file exists in the data folder. This is intended
% to protect the user from overwritting non (binary) identical files. The
% reason for this is that two hdf files with the same data are not identical
% when recreated (see line 11). Usually you would use an existing file
% without changing it.