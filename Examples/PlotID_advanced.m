clearEnv;
%%  Tag and export multiple plots
% here the work flow for tagging and exporting multiple plots is shown

% plots and data
fig(1) = figure;
[x1, y1, datapath1] = createExampleData('matlab');
plot(x1,y1,'-b'); box off; hold on; set(gca, 'TickDir', 'out', 'YLim', [0,4]);

fig(2) = figure;
[x2, y2, datapath2] = createExampleData('hdf');
plot(x2,y2,'-r'); box off; set(gca, 'TickDir', 'out', 'YLim', [0,4]);

%% 1. Tag both plots 
[fig, IDs] = PlotID.TagPlot(fig);

% data locations
script = [mfilename('fullpath'),'.m']; % filename of the m.script
% file names of the datasets
rdata =  {datapath1, datapath2} ; 

%% 2. publish via a for-loop
for i=1: numel(fig)
    PlotID.Publish(rdata{i}, script, fig(i));
end

%% Debugging
% Publish has two options that helps you with debugging:
% you can activate messages from each step of Publish with
% 'ShowMessages', true
% you can force publish to skip "Soft errors" with the option
% 'ForcePublish', true 
% and check the output folder