function [x,y, fpath] = createExampleData(option)
%CREATEEXAMPLEDATA creates x,y data needed for the PlotID examples
% fpath contains the file path of the example data
% there are two options, the option 'matlab' creates a matlab file and the
% hdf option a hdf5 file

switch option
    case 'hdf'
        if isfile('test_hdf_data.h5')
           % hdf files can not simply be overwritten
            delete test_hdf_data.h5;
        end        
        % some data for the .h5 file
        x = linspace(0,2*pi); y = sin(x)+2;

        % define filepath & name
        dataset = 'test_hdf_data.h5';
        dataset = fullfile(pwd,dataset);
        fpath = dataset;

        % create hdf5 file and dataset > write data to hdf5 file / dataset
        h5create(fpath, "/x", size(x), "Datatype", class(x))
        h5create(fpath, "/y", size(y), "Datatype", class(y))
        h5write(fpath, "/x", x)
        h5write(fpath, "/y", y)
    case 'matlab'
        % Creating random data to use as data-file
        x = linspace(0,7); y = rand(1,100)+2;
        dataset = 'test_data.mat';
        % Use absolute paths for good practise
        fpath = fullfile(pwd,dataset);
        save(dataset,'x','y');
    otherwise
        error('option not defined')
end

end

