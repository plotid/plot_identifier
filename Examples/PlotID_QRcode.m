clearEnv;
%%  Tag the plot with a QR code (experimental)
% here the work flow for tagging the plot with a QR code is shown
% the content of the qr code is the ID

% plots and data
fig = figure;
[x1, y1, datapath1] = createExampleData('matlab');
plot(x1,y1,'-b'); box off; hold on; set(gca, 'TickDir', 'out', 'YLim', [0,4]);


%% 1. Tag plot with QRcode
% QR size is the relative size of the QR code (0.15 default)
[fig, IDs] = PlotID.TagPlot(fig,'Location','southeast','QRcode',true, 'QRsize', 0.18);
