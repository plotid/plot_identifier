%% This is the PlotID Initialisation file
% Please run this script before using PlotID

%% Section 1 Config File
% To make things easy, you should use the config file. The following wizard
% will help you create one:
writeConfig = true;


% Check for matlab version
if verLessThan('matlab','9.10')
    msg="Your matlab Version is lower than 9.10 (2021a). The config file cannot be created with this wizard. You can either input data manually for each export or create the config file manually. For further Information see the Readme. More function might be impaired when using an old Matlab Version.";
    error(msg);
end
% Ask User if current config should be overwritten
if isfile('config.json')
    m = input('Do you want to overwrite the current config file, Y/N [Y]:','s');
    writeConfig = ismember(m,{'Y','y'});
end

if writeConfig
   config = PlotID.config.wizard('initialise');
   fid = fopen('config.json','w');
   txt = jsonencode(config,'PrettyPrint',true);
   fwrite(fid,txt);
   fclose(fid);
end

%%
clc
disp('Initialisition done.')
