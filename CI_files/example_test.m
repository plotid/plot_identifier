function [result] = example_test()
%   example This is a test if all PlotID example works run through

% prevent function to be affected by clear commands in examples
ciTest = true; 

% set path
% starting path of gitlab runner is in CI_files
if contains(pwd,'CI_files')
    cd .. % move one directory up
end

addpath('CI_files','Examples');

% clean up, if previous run failed
try
    delete(['CI_files' filesep 'PlotID_export' filesep '*']);
    delete(['CI_files' filesep '*.mat']);
    delete(['CI_files' filesep '*.h5']);
    rmdir(['CI_files' filesep 'PlotID_export'],'s');
end


exampleList = {'PlotID_example','PlotID_advanced',...
     'PlotID_QRcode', 'PlotID_centralized','PlotID_variables'};


% initialise
numberOfTests = numel(exampleList);
testResults = zeros(numberOfTests,1);

%start log
fid = fopen(fullfile('CI_files','log.txt'),'w');
txt = ['default test started ' newline];
fprintf(fid,txt);

% create Config for CI-Tests
fid1 = fopen(fullfile('CI_files','config.json'),'w');
configData.Author = 'CI-Test'; configData.ProjectID = 'CI-001';
txt = jsonencode(configData,'PrettyPrint',true);
%fprintf does not write paths correctly !!!
fwrite(fid1,txt);
fclose(fid1);

%% Testing Loop
for i=1:numberOfTests
    clearvars -except i fid exampleList testResults
    try
        runExample(exampleList{i});
        msg = ['example_test succeed example ', exampleList{i}];        
    catch
        testResults(1) = 1; %test fails
        msg = ['example_test failed at example', exampleList{i}];
        warning(msg);
    end
    
    fprintf(fid,[msg, newline]);
end


%% Test result
if any(testResults)
    result = 1; %fail
    warning('test failed');
else
    result = 0; % pass
    disp('test succeed');
end

fclose(fid);

% final clean up
try
    delete(['CI_files' filesep 'PlotID_export' filesep '*']);
    delete(['CI_files' filesep '*.mat']);
    delete(['CI_files' filesep '*.h5']);
    rmdir(['CI_files' filesep 'PlotID_export'],'s');
    delete(fullfile('CI_files','CI_config.json'));
end

end

function runExample(scriptName)
% runExample prevents scripts to infulence the CI script
    run(scriptName);
    
end

