function [result] = default_test()
%default_test() This is a simple test if Plot ID works for the default settings
%   Detailed explanation goes here

% set path
% starting path of gitlab runner is in CI_files
if contains(pwd,'CI_files')
    cd .. % move one directory up
end

addpath('CI_files');

% clean up, if previous run failed
try
    delete(['CI_files' filesep 'PlotID_export' filesep '*']);
    delete(['CI_files' filesep '*.mat']);
    delete(['CI_files' filesep '*.h5']);
    rmdir(['CI_files' filesep 'PlotID_export'],'s');
end

% initialise
numberOfTests = 2;
testResults = zeros(numberOfTests,1);

%start log
fid = fopen(fullfile('CI_files','log.txt'),'w');
txt = ['default test started ' newline];
fprintf(fid,txt);

% create Config for CI-Tests
fid1 = fopen(fullfile('CI_files','CI_config.json'),'w');
configData.Author = 'CI-Test'; configData.ProjectID = 'CI-001';
txt = jsonencode(configData,'PrettyPrint',true);
%fprintf does not write paths correctly !!!
fwrite(fid1,txt);
fclose(fid1);

try
    ProjectID = 'Test01';
    %% Data
    % some random data
    x = linspace(0,7);
    y = rand(1,100)+2;
    dataset1 = fullfile('CI_files','test_data.mat');
    save(dataset1,'x','y');
    % some data as .h5
    x1 = linspace(0,2*pi);
    y1 = sin(x1)+2;

    % define file path & name
    fpath = fullfile("CI_files","testdata2.h5");
    dataset2 = fullfile('CI_files','testdata2.h5');

    % create hdf5 file and dataset > write data to hdf5 file / dataset
    h5create(fpath, "/x1", size(x1), "Datatype", class(x1))
    h5create(fpath, "/y1", size(y1), "Datatype", class(y1))
    h5write(fpath, "/x1", x1)
    h5write(fpath, "/y1", y1)

    %% Plotting

    fig(1) =figure('visible','off');
    plot(x,y,'-k');
    hold on
    plot(x1,y1,'-r');
    msg = 'simple_test succeed stage 1';
catch
    testResults(1) = 1; %test fails
    msg = 'simple_test failed in Stage 1';
    warning(msg);
end

fprintf(fid,[msg newline]);

%% Tag the plot
try
    [figs, ID] = PlotID.TagPlot(fig,'ConfigFileName', 'CI_config.json');

    %% call a dummy function
    %a=1;
    %a = example_fcn(a);

    %% publishing

    % The functions needs the file location, the location of the data and the
    % figure
    script = [mfilename('fullpath'),'.m']; % filename of the m.script

    % file name of the data
    rdata =  {dataset1,dataset2} ; % don't forget the extension

    PlotID.Publish(rdata,script, figs, 'Location', 'CI-Test',...
        'ConfigFileName', 'CI_config.json');

    msg = 'simple_test succeed Stage 2';

catch
    testResults(2) = 1; %fail
    msg = 'simple_test failed in Stage 2';
    warning(msg);
end
fprintf(fid,[msg newline]); %log

%% Test result
if any(testResults)
    result = 1; %fail
    warning('test failed');
else
    result = 0; % pass
    disp('test succeed');
end

fclose(fid);

% final clean up
try
    delete(['CI_files' filesep 'PlotID_export' filesep '*']);
    delete(['CI_files' filesep '*.mat']);
    delete(['CI_files' filesep '*.h5']);
    rmdir(['CI_files' filesep 'PlotID_export'],'s');
    delete(fullfile('CI_files','CI_config.json'));
end

end

