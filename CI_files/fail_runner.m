function [retVal] = fail_runner()
%RUNNER_TESTING testing function to test, if the runner is set up properly
    retVal = 1; % test should fail !
    fid = fopen(fullfile('log.txt'),'w');
	txt = ['This is a test, Errorstate: ', num2str(retVal)];
    fprintf(fid,txt); 
    fclose(fid);
end
