#!/bin/bash

#matlab -r "disp(['Current folder: ' pwd])"
matlab $@ -nodisplay -nodesktop -nosplash -logfile matlab_log.txt -r "default_test;exit(ans);"

exitstatus=$?
if [[ $exitstatus -eq '0' ]]
then 
    echo "matlab succeed. Exitstatus: $exitstatus"
    exit $exitstatus
else
    echo "matlab failed. Exitstatus: $exitstatus"
    exit $exitstatus

fi
