function [IDf,PrjID, Str] = friendlyID(ID)
% FriendlyID Changes the Hex Number to a human friendly datetime and dateStr
%
%   IDf ID as DateTime Object, PrjID returns the ProjectID, Str returns the
%   timestamp as String

%Remove Prefix
if contains(ID,'-')
   IDarray = split(ID, '-');
   PrjID = IDarray{1};
   ID = IDarray(2); %keep only the second part
else
   PrjID = '';
end

ID = hex2dec(ID); % get it back as dec
IDf = datetime(ID,'ConvertFrom','posixtime');
Str = datestr(IDf);

end

