function [figs, IDs] = TagPlot(figs, options)
% TagPlot adds IDs to figures
%   The ID is placed visual on the figure window and as Tag (property of figure)
%   TagPlot can tag multiple figures at once.
%   If a single Plot is tagged IDs is a char, otherwise it is a cell array of
%   chars 
%   options.ProjectID is the project number (string or char), if empty the ID from the
%   config file is used
%   
%   The ID is placed on the 'east' per default, if you want it somewhere
%   else, use the 'Location' option. 'north','east','south','west' are
%   predefined, otherwise use the 'custom' option and provide the desired
%   'Position' (relative to your x- and y-axis limits)
%   'ConfigFileName' is the config-file which is used for the ProjectID
%   If you use multiple config files you need to use this option.
arguments
    figs (1,:) {mustBeFigure}
    options.ProjectID (1,:) {mustBeText}= ''
    options.Fontsize (1,1) {mustBeInteger} = 8
    options.Color (1,3)  {mustBeNonnegative} = 0.65*[1 1 1] % grey
    options.Location (1,:) {mustBeMember(options.Location,{'north','east',...
        'southeast','south','west','custom'})} = 'east'
    options.DistanceToAxis {mustBeReal} = .02 % relative distance
    options.Position (1,2) {mustBeVector} = [1,0.4] % default for east
    options.Rotation (1,1) {mustBeReal} = NaN
    options.ConfigFileName (1,:) {mustBeText} = 'config.json'
    options.PinToLegend (1,1) {mustBeNumericOrLogical} = false % Pins ID on Legend 
    options.QRcode (1,1) {mustBeNumericOrLogical} = false %experimental
    options.QRsize (1,1) {mustBeNonnegative} = 0.15 % size of the QRCode
end

if isempty(options.ProjectID)
   configObj = PlotID.config(options.ConfigFileName);
   configData = configObj.configData;
   options.ProjectID = configData.ProjectID;
end

switch options.Location
    case 'north'
        y = 1 - options.DistanceToAxis;
        options.Position = [0.4,y];
        Rotation = 0;
    case 'east'
        x =  1 - options.DistanceToAxis;
        options.Position = [x,0.4];
        Rotation = 90;
    case 'south'
        y = 0 + options.DistanceToAxis;
        options.Position = [0.4,y];
        Rotation = 0;
    case 'west'
        x =  0 + options.DistanceToAxis;
        options.Position = [x,0.4];
        Rotation = 90;
    case 'southeast'
        y = 0 + options.DistanceToAxis;
        options.Position = [0.8, y];
        Rotation = 0;
    case 'custom'
        % Check if Position is valid
        if ~all(0 <= options.Position & options.Position <= 1)
           options.Position = [1,0.4];
           warning('options.Position is not valid, TagPlot uses default values instead');
        end
    otherwise % set default position
        warning([options.Location, ' is not a defined location, TagPlot uses location east instead']);
        options.Location = 'east'; options.Position = [1,0.4];
end

if ~isnan(options.Rotation)
   Rotation = options.Rotation; 
end

IDs = cell(numel(figs),1);

for n = 1:numel(figs)
    IDs{n} = PlotID.CreateID; % Create ID
    IDs{n} = [options.ProjectID,'-',IDs{n}]; % add options.ProjectID
    pltAxes = get(figs(n),'CurrentAxes'); % Axes object for text annotation
    % Limits for relative positioning
    ylim =get(pltAxes,'YLim');
    xlim =get(pltAxes,'XLim');   

    if options.PinToLegend
        if options.QRcode
            warning(['PinToLegend and QRCode can not be used at the same time',...
                newline, 'QRcode is deactivated.']);
            options.QRcode = false;
        end
        lobj = findobj(figs(n), 'Type', 'Legend');
        if isempty(lobj)
            set(0, 'CurrentFigure', figs(n))
            lobj = legend();
        end 
        % new position based on legend
        posVec = get(lobj,'Position');
        options.Position(1) = posVec(1)+.04;
        options.Position(2) = posVec(2);
    end

    % add text label
    position = [options.Position(1), options.Position(2)];    
    t=text(pltAxes,position(1),position(2), IDs{n},'Fontsize',options.Fontsize,...
    'Rotation',Rotation, 'VerticalAlignment','bottom','Color',...
        options.Color,'BackgroundColor','w', 'Units', 'normalized');
    set(figs(n),'Tag', IDs{n}); 


    if options.QRcode
        % this should be seen and use as a proof of concept
        qrCode = PlotID.plotQR(IDs{n});
        size = options.QRsize;
        axes('Position',[position(1)-.05 position(2)+0.1 size size]);
        imshow(qrCode);
        t.Visible = 'off';
    end


end

if numel(figs) == 1
   % use char instead of a cell arry for a single ID
   IDs = IDs{1}; 
end

end

function tf = mustBeFigure(h) %checks if input is a figure object
  tf = strcmp(get(h, 'type'), 'figure') & isa(h, 'matlab.ui.Figure');
end
