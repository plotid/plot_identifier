function [status] = createLinkedHDF5(SourceFile,TargetPath)
% createLinkedHDF5 creates a HDF file that references the Sourcefile
%   TargetPath is the storage location
%   Status returns true if the function was successfull

plist_id = 'H5P_DEFAULT';

%catches error of wrong file type
if iscell(SourceFile)
    SourceFile = SourceFile{1};
end

[~,filename,ext] = fileparts(SourceFile);

% create GroupName to avoid internal path issues on linux/unix systems
GroupName = split(SourceFile,filesep);
% Using windows fileseperator for the object name
GroupName = join(GroupName,'\');
GroupName = GroupName{:};

try
    fid = H5F.create(fullfile(TargetPath,[filename,ext]));
    %create External Link to Sourcefile in the Group linkToExternal
    H5L.create_external(['..',SourceFile],'/',fid, GroupName ,plist_id,plist_id);
    %H5L.create_external(['..,filesep,'data',SourceFile],'/',fid, GroupName ,plist_id,plist_id); %original
    H5F.close(fid);
    disp([fullfile(TargetPath,[filename,ext]),' created']);
    status = 1;
catch
    warning('No linked HDF file was created');
    status = 0;
end

end
