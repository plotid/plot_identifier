function img = plotQR(qrTxt, size)
% plotQR creates a QR code based on qrTxt by calling a qrserver (depends on
% API)
%   size specifies the absolut size of the qr code (pixels)
%   special thanks to J.Stifter for this suggestion

    arguments
        qrTxt {mustBeTextScalar} = 'example';
        size (1,2) {mustBeInteger} = [150, 150];
    end
    
    m = size(1);
    n = size(2);
    
    base_api = 'https://api.qrserver.com/v1/create-qr-code/?size=%dx%d&data=%s';
    request = sprintf(base_api, m, n, qrTxt);
    
    options = weboptions;
    options.Timeout = 5;
    
    img = webread(request, options);
    img =logical(img);

end

