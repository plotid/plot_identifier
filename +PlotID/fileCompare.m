function [status, id] = fileCompare(filename,fileList)
% fileCompare checks if file1 is (binary) identical to a file in filelist
% it returns a status and the id of the identical file
%
%   the functions uses the java libraries from matlab for windows systems
%   or the unix function diff because of performance issues with windows
%   system calls in matlab

if isempty(fileList)
   % no comparison necessary
    status =false;
    id = 0;
    return
end

[~,~,ext1] = fileparts(filename); 
id = zeros(height(fileList),1);

for i=1:height(fileList)
    [~,~,ext2] = fileparts(fileList{i,'name'});

    if ~isequal(ext1,ext2)
        %warning('File extension are not identical');
        status = false;
        continue
    end

    filepath = fullfile(fileList{i,'folder'},fileList{i,'name'});
    
    %% comparison with java function
    % TODO test paths with spaces
    status = comparejava(filename, char(filepath));
    
    if status == 1        
        id(i) = 1;
        id =logical(id); %bugfix
        return;
    else 
        % Status can also be any other number e.g. 2 
        id(i) = 0;
    end
    
    id =logical(id); %bugfix
end

end

function is_equal =  comparejava(file1, file2)
%% Utilizing java functionalities for inter-os comparison of binary files
% file1 and file2 should be full filepaths

file1 = javaObject('java.io.File',file1);
file2 = javaObject('java.io.File',file2);
is_equal = javaMethod('contentEquals','org.apache.commons.io.FileUtils',file1,file2);

is_equal = double(is_equal); % Conversion for compatibility reasons
end
