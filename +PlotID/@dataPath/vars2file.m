function obj = vars2file(obj,isStruct)
%vars2file creates a temporary file for all variables in inputPaths
% the number of inputPaths is reduced by the number of variables

tmpEnv= getenv('TMP');
tmpDir = fullfile(tmpEnv,'PlotID');
if ~isfolder(tmpDir)
    mkdir(tmpDir);
end
obj.tmpPath = fullfile(tmpDir,[obj.ID,'_vars.mat']); 

% save each of the structs 
for i=1:numel({obj.DataPaths{isStruct}})
    struct = obj.DataPaths{isStruct};
    if i==1
        save(obj.tmpPath,'-struct','struct');
        obj.tmpPath = {obj.tmpPath};
    else
        tmpPath = fullfile(tmpDir,[obj.ID,'_vars',num2str(i),'.mat']);
        save(tmpPath,'-struct','struct');
        obj.tmpPath{end+1}=tmpPath;
    end
end

% remove variable from Datapath
obj.DataPaths(isStruct) = [];
% add tmppath as Datapath

obj.DataPaths = horzcat(obj.DataPaths, obj.tmpPath);

end

