classdef dataPath < handle
    % DATAPATH stores the datapaths to the research data
    %   usage of a class is neccessary to support variables, argument
    %   validation and storing variables as tempory files
    
    properties (SetAccess = protected)
        DataPaths (1,:) cell % dataPaths
        tmpPath  % path to TMP files 
        ID %PlotID
        dataFolderName = 'data' %name for the data folder when using centralized
    end
    
    methods
        function obj = dataPath(inputPaths,ID)
            %DATAPATH Construct an instance of this class
            %   start with argument validation
            obj.ID = ID;
            
            %catch non cell inputs in inputPaths 
            if ~iscell(inputPaths)
                  inputPaths = {inputPaths}; %Cell array
            end
            


            % handle nested cell arrays
            while any(cellfun(@iscell,inputPaths)) % while any of the cells contain cells
                % concatenate the content of the cells containing cells
                % with the ones that don't contain cells
                inputPaths = [inputPaths{cellfun(@iscell,inputPaths)} inputPaths(~cellfun(@iscell,inputPaths))];
            end

            isStruct = false([1,numel(inputPaths)]);
            % strings will cause problems, therefore chars are used 
            for i=1:numel(inputPaths)
                if isstring(inputPaths{i})
                   inputPaths{i} = char(inputPaths{i});
                end
                % check for Variable inputs           
                if isstruct(inputPaths{i})
                    isStruct(i) = true;
                elseif ~ischar(inputPaths{i})
                    obj.throwError(); 
                end
            end
                        
            obj.DataPaths = inputPaths;
            % create temporary file from all Variables
            if any(isStruct)
               obj.vars2file(isStruct); 
            end

            % final check if all paths are valid
            mustBeDataPath(obj);
        end

         function mustBeDataPath(obj)
            if ~isempty(obj.DataPaths)
                %checks if input is a valid DataPath object
                  tf = ~isfile(obj.DataPaths);
                  if any(tf)
                     obj.throwError();
                  end
            end
        end 
              
        
        function cleanTmpFile(obj)
            if ~isempty(obj.tmpPath)
                delete(obj.tmpPath{:}); 
            end
        end

        %% non local functions
        vars2file(obj,isvar);
    end
    methods (Static)
        function throwError()
        %THROWERROR throws an error Dialog for invalid Data
          eidType = 'mustBeDataPath:notaValidFilePath';
          msgType = 'DataPaths must contain file-path(s) or Variables';
          throwAsCaller(MException(eidType,msgType))
        end 
    end %static
end %class

