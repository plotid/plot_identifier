function pList = copyUserFCN(scriptPath, folderName, storPath, ID)
% COPYUSERFCN copies all user functions, classes and toolboxes that are used
% by a script (scriptpath).
%
%   All toolboxes and classes are copied as a whole.
%   folderName, storPath and ID are required for createfilecopy
%   see also createFileCopy

   [fList,pList] = matlab.codetools.requiredFilesAndProducts(scriptPath);
   fList = fList(~ismember(fList,scriptPath)); % rmv plot script itself from list
   fList = fList(~contains(fList,'config.json')); % rmv config.json from list
   fList = removePltIdFiles(fList); % Do not copy files that are part of PlotID
   
   % copy Classes and Toolboxes as a whole
    copyTBorClass(fList,'+', folderName, storPath, ID); % Toolboxes 
    copyTBorClass(fList,'@', folderName, storPath, ID); % Classes
    %remove class and toolbox files from flist
    fList = fList(~contains(fList,{'@','+'}));
    
    % copy User FCN
   if ~isempty(fList)
       PlotID.createFileCopy(fList,folderName,storPath,ID,'userFcn');
   end
end

function [fListClean] = removePltIdFiles(fList)
    %removePltIdFiles removes functions that are part of PlotID out of flist
    %   Detailed explanation goes here
    
    [~,names,ext] = fileparts(fList);
    names = strcat(names, ext); % add ext for comparison
    
    % Get a list of all .m files that are part of Plot id
    packageContent = what('PlotID');
    % packageContent.classes has no extensions
    PltID_classlist = packageContent.classes;
    
    % Class Methods need to be listed in an additional function
    Class_flist = cell(1,numel(PltID_classlist)); %preallocate 
    for i=1:numel(PltID_classlist)
        temp = what(['PlotID',filesep,'@',PltID_classlist{i}]);
        Class_flist{i} = temp.m; 
    end
     
    Class_flist = vertcat(Class_flist{:});
    % all plotID .m files:
    PltID_flist = [packageContent.m; Class_flist]; 
    % Comparison and filter
    fListClean = fList(~ismember(names,PltID_flist));
end

function [status, msg] = copyTBorClass(fList,leadChar, folderName, storPath, ID)
%copyTBorClass copies the Toolboxes or Classes that are part of flist 
% lead char must be either '@' for Classes or '+' for Toolboxes
if any(contains(fList,leadChar))
    list = fList(contains(fList, leadChar));
    names = extractBetween(list,leadChar,filesep);
    paths = extractBefore(list,strcat(leadChar,names));
    fullPaths = strcat(paths,strcat(leadChar,names));
    fullPaths = unique(fullPaths);
    [~, status, msg] =PlotID.createFileCopy(fullPaths,...
        folderName,storPath,ID,'class'); 
end %if
end