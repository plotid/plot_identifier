function replaceLinkedHDF5(linkedFilepath)
% replaceLinkedHDF5 replaces the HDF5, that contains only a link to another
% HDF5 File with a file containing the actual Data, thus turning it into an
% independent data file. It essentially turns the plotID option
% 'centralized' into 'individual'.
%   Filepath is the location of the File that contains the link. The
%   filepath to the linked data has to be present in the file itself.

%% Check whether the only Objects present are links
h5inf = h5info(linkedFilepath);

% default behaviour dont overwrite
overwrite = false;

if isempty([h5inf.Groups, h5inf.Datasets, h5inf.Datatypes, h5inf.Attributes]) && ...
      all(size(h5inf.Links) == [1 1])  
    % There are no other known objects present and
    % there is only one linked file
       overwrite = true;
end

%% Create Copies of files
for i = 1:size(h5inf.Links, 2)
    % This is the file with full data  
    linkTargetPath = h5inf.Links(i).Value{1,1}; 
    % This is the empty file
    % TODO filename decision
    linkSourcePath = strcat(linkedFilepath,'_',num2str(i)); 

    % Create an absolute path from the relative linking path
    absoluteTP = regexp(linkSourcePath, filesep, 'split');
    TargetPathSplit = regexp(linkTargetPath, filesep, 'split');
    % How many levels do we need to go up in the relative path
    % This is merely counting the occurances of '..' within the path, but
    % should not cause issues if the realtive path was created by the
    % linking process
    levelsUp = sum(contains(TargetPathSplit,'..'));
    absoluteTP = absoluteTP(1:(end-(1+levelsUp)) ); %Remove filename + levels
    linkTargetPath = strjoin([absoluteTP, TargetPathSplit(~contains(TargetPathSplit,'..'))],filesep);
    
    if ~isfile(linkTargetPath)
        warning(strcat("Link destination """,linkTargetPath,""" does not exist or relative path does not fit, no copy created"))
    end
    
    if overwrite % can only be for i=1, and size = 1
    linkSourcePath = linkedFilepath;
    end
    disp('Copying file from ', linkSourcePath, ' to ', linkTargetPath)
    copyfile(linkTargetPath,linkSourcePath);
    
end
        
        
    
end




