function Publish(DataPaths,scriptPath, figure, options)
% Publish(DataPaths,scriptPath, ID, figure, options) saves plot, data and measuring script 
%
%   DataPaths contains the path(s) to the research data, for multiple files
%   use a cell array or ONE struct of variables (only the first struct will
%   be exported)
%   scriptPath contains the path to the script, this can be provided with
%   the simple call of scriptPath = [mfilename('fullpath'),'.m']
%
%   Options:
%   Location sets the storage location. 'local' sets the storage location
%   to the current folder (an export folder will be created), 'manual' opens
%   an explorer window, where you can choose the folder. If you define a export
%   path in the config file, this will used per default (exportPath).
%   remote path, that is defined in the config file.
%   Two Methods are implemented 'individual' stores the data for
%   each plot while 'centralized' uses a data folder and uses reference links
%   to the original data (hdf5 only).
%   'ParentFolder' is the folder name where the exported data is stored if an
%   path is used, PlotId will use this path as storagePath
%   'ConfigFileName' is needed for handling multiple config files (see example)
%   'CSV' turns a summary table of all exports on or off 
%
%   DebugOptions:
%   'ShowMessage' and 'ForcePublish' are mainly for debugging. 
%   'ShowMessage' will display Messages for every step of PlotID,
%   'ForcePublish' will publish even if one step was not successful (not recommended) 

arguments
   DataPaths % location of the data-file(s) and/or ONE struct of variables
   scriptPath (1,:) {mustBeText} % location of the matlab script
   figure (1,:) {mustBeFigure} % Checks if figure is a figure object
   options.Location {mustBeMember(options.Location ,{'local','exportPath','manual','CI-Test'})} = 'local' % storage path
   options.Method {mustBeMember(options.Method ,{'individual','centralized'})} = 'individual'
   options.ParentFolder (1,:) {mustBeText} = 'PlotID_export' % name of the ParentFolder
   options.ConfigFileName (1,:) {mustBeText} = 'config.json' %individual config names possible
   options.CopyUserFCN (1,1) {mustBeNumericOrLogical} = true
   options.CSV (1,1) {mustBeNumericOrLogical} = true
   options.ShowMessages(1,1) {mustBeNumericOrLogical} = false 
   options.ForcePublish (1,1) {mustBeNumericOrLogical} = false %publish anyway
end

%% argument validation 
% strings will cause problems, therefore chars are used 
if isstring(scriptPath)
   scriptPath = char(scriptPath);
end

% ensure ParentFolder has no trailing fileseperator
if contains(options.ParentFolder(end),filesep)
    options.ParentFolder(end)='';
end

%catch multiple figures in fig
if numel(figure) > 1
    figure = figure(1);
    msg = ['Publish is designed for handling one figure at once' newline,...
        '- publishes uses only the first figure.' newline , ...
        'Consider an external for loop for multi figure export as provided in example.m'];
    warning(msg);
end

% find scriptpath
if ~isfile(scriptPath)
    scriptPath= which(scriptPath);
    if ~isfile(scriptPath)
        msg = ['Script path: ',scriptPath, ' not found on matlab path', newline, ...
            '- check spelling or us a path']; 
        error(msg);
    end
end

% catch missing .m extension
[~,~, scriptExt] = fileparts(scriptPath);
if isempty(scriptExt)
    scriptPath = [scriptPath,'.m'];
end

%get ID from Figure
ID = figure.Tag;

if isempty(ID)  
   % no ID found, user dialog for folder name 
   ID = inputdlg(['No ID defined- ' newline,...
       'Please enter a folder name to continue:'],'Please enter a folder name');
   ID = ID{1};
   msg = ['No ID found - consider to use the TagPlot function before ',...
       'you publish ', newline, 'your files will be stored in ' , ID]; 
   warning(msg);
end

%% Create a Datapath object from Input
dataObj = PlotID.dataPath(DataPaths,ID);

%% read config file
% there is only one config Object (handleClass)
configObj = PlotID.config(options.ConfigFileName);
% read user options from config and set them for Publish
if isfield(configObj.configData, 'options')
   fldnames = fieldnames(configObj.configData.options);
   for ii=1:numel(fldnames) 
      field = fldnames{ii};
      options.(field) = configObj.configData.options.(field);
   end
end

% error and MSG handling object
dlgObj = PlotID.userDLG(ID,options); 

%% storage location
switch options.Location 
    case 'local'
        if contains(options.ParentFolder, {'/','\'})
            storPath = options.ParentFolder;
        else
            % use the script path as export path
            scriptLocation = fileparts(scriptPath);
            storPath = fullfile(scriptLocation,options.ParentFolder);
        end    
    case 'exportPath' 
        if isfolder(configObj.exportPath)
           storPath = configObj.exportPath;
        else
           msg = ['Your Export folder ', configObj.exportPath, newline,...
                'does not exist, check the config file - publishing  not possible!'];         
           dlgObj.error(msg);
        end
    case 'manual' %UI
        storPath = uigetdir();
    case 'CI-Test' %only for CI tests
        storPath = fullfile(pwd,'CI_files',options.ParentFolder);
end

folderName = ['.',char(ID)]; % hidden folder
folderNameV = char(ID); %visible Folder

%% Create data directory
overwriteDir = false;
% if invisible Folder exists, delete it (publish was not successful before)
if isfolder(fullfile(storPath,folderName))
   rmdir(fullfile(storPath,folderName),'s')
end
% if folder already published: ask User
if isfolder(fullfile(storPath,folderNameV))
   msg = ['Folder ',folderNameV, ' exists - Plot was already published '];
   disp(msg);
   dialogMsg = 'Do you want to overwrite the existing files';
   overwriteDir = dlgObj.userQuestion(dialogMsg);
   if ~overwriteDir % USR does not want to overwrite
       warning('PlotID has finished without an export');
       disp('rerun TagPlot if you need a new ID or consider overwriting.');
       return; %terminate
   end
end
% create the folder
if ~mkdir(fullfile(storPath,folderName))
    dlgObj.error('Directory could not be created - check remote path and permissions');
end
disp(['publishing of ', ID, ' started']);

%% Create a copy of the script and user functions(optional)
% script
[~, status, msg] = PlotID.createFileCopy({scriptPath},folderName,storPath,ID, 'script');
dlgObj.scriptPublished =status;
dlgObj.userMSG(msg);

% user functions
if options.CopyUserFCN
   toolboxList = PlotID.copyUserFCN(scriptPath, folderName, storPath, ID);
end

%% Research data handling
switch options.Method
    case 'centralized' 
        [status, msg] = PlotID.centralized(storPath, dataObj, folderName);
        
    case 'individual'
        % Create a copy of the research data    
        [~, status, msg] = PlotID.createFileCopy(dataObj.DataPaths,folderName,storPath,ID, 'data');
end
%temporary:
dlgObj.rdFilesPublished = status;
dlgObj.userMSG(msg);

%% Write Config File
exportPath = fullfile(storPath,folderName);
configObj.writeConfig(exportPath);

% add further Metadata
meta = struct();
if ispc
    meta.author = getenv('USERNAME');
end
meta.ProjectID = ID;
meta.CreationDate = datestr(now);
meta.MatlabVersion = version;
if options.CopyUserFCN  
    meta.ToolboxVersions = toolboxList;
end
% write meta
metaPath = fullfile(storPath,folderName,'plotID_data.json');
fid = fopen(char(metaPath),'w');
txt = jsonencode(meta,'PrettyPrint',true);
fprintf(fid,txt); 
fclose(fid);


%% Export the Plot
try 
   PlotName = [ID,'_plot']; % plotname
   RemotePath = fullfile(storPath ,folderName, PlotName);
   % Matlab figure
   savefig(figure,RemotePath);
   % the png should only be a preview
   exportgraphics(figure,[RemotePath,'.png'],'Resolution',300);
   dlgObj.figurePublished = true; 
catch 
   dlgObj.softError('Plot export was not successful');
end

%% final renaming and error/warning handling
% if no error occurred or if force publish is activated, rename the hidden
% folder to a non hidden one, otherwise delete it.
if dlgObj.success || options.ForcePublish
    oldPath = fullfile(storPath,folderName);
    newPath = strrep(oldPath,'.',''); %remove dot
    if overwriteDir
       rmdir(newPath,'s');
       dlgObj.userMSG(['old export ', folderNameV, ' deleted']); 
    end
    status = movefile(oldPath,newPath); %rename directory
end
% delete tmp data
dataObj.cleanTmpFile();

%% CSV export
if options.CSV
  T = table();
  T.research_Data = dataObj.DataPaths'; T.PlotID(:) = {ID};
  T.Author(:) = {configObj.configData.Author};
  T.Script_Name(:) = {scriptPath};
  T.Storage_Location(:) = {newPath};
  T.Date(:) = {datestr(now)};
  T = movevars(T,{'PlotID','Author'},'before',1);
  writetable(T, fullfile(storPath, 'PlotID_overview.csv'),'WriteMode','append');
end

if status 
    disp(['publishing of ', ID , ' to ', newPath, ' done']); %always displayed on success   
else % publish was not successful! 
    dlgObj.error(['publishing of ', ID , ' failed'])
end


end %function

%% Argument Validation Functions
function tf = mustBeFigure(h)
%checks if input is a figure object
  tf = strcmp(get(h, 'type'), 'figure') & isa(h, 'matlab.ui.Figure');  
end



