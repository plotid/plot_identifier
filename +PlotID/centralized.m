function [status, msg] = centralized(storPath, dataObj, folderName)
% CENTRALIZED stores the research files in a centrelized folder.
%   Linked files are created for subsequent folders (HDF5 only).
%
%   storPath for the Data
%   dataObj contains tha data related information
%   folderName is the Name of the storage Folder
msg = '';
warning(['Linked HDF5 can only be moved with their ',...
    'respective master files in the ', dataObj.dataFolderName, ' folder.']);

% check if data folder exists
if ~isfolder(fullfile(storPath,dataObj.dataFolderName))
    mkdir(fullfile(storPath,dataObj.dataFolderName));
end
% to get relative Paths
currentPath = fullfile(storPath);

%list all files
fList = dir(fullfile(storPath,dataObj.dataFolderName, ['**',filesep,'*.*']));
%get list of files and folders in any subfolder
fList = fList(~[fList.isdir]);  %remove folders from list
fList = struct2table(fList);
        
% Check if the new plot is based on the original data-set    
    % copy the data(once)
for i=1:numel(dataObj.DataPaths)            
    % check if identical file exists (status = 1)
    [~, idx] = PlotID.fileCompare(dataObj.DataPaths{i},fList);
    % create l inked HDF5 files for identical files
    if any(idx)
        fList.path = fullfile(fList.folder,fList.name);
        sourcePath = fList{idx,'path'};
        if ~iscell(sourcePath)
            sourcePath = {sourcePath};
        end
        relativeSourcePath = strrep(sourcePath,currentPath,'');
                        
        if contains(sourcePath,{'.h5','.hdf5'}) % Linking only for HDF5
            linkedHDFPath = fullfile(storPath,folderName);
           [status] = PlotID.createLinkedHDF5(relativeSourcePath{1,1},linkedHDFPath);
        else % Non-HDF5 file already exists
            [status] = true;
        end
    else % no identical file exists
        %Copy the file in data and create the links (if hdf5)
        [dataPath, status, msg] = PlotID.createFileCopy(dataObj.DataPaths{i},...
            'data',storPath,dataObj.ID,'dataCentral');
        pathToData = strrep(dataPath,currentPath,'');
        
        if ~status
           return; % an error orccured
        end

        %WIP
        if contains(dataObj.DataPaths{i},{'.h5','.hdf5'}) % Linking only for HDF5
           % and create also linked files in the plot folder 
           linkedHDFPath = fullfile(storPath,folderName);
           [status] = PlotID.createLinkedHDF5(pathToData,linkedHDFPath);
        else
            warning(['You use the centralized method for non hdf files,',...
                newline, 'Your research files are located in the ',...
                DataFolderName , 'folder.']);
            status = true;
        end  %if
    end %if
    % add do not move message
    doNotMove = ['do not move this folder without the ',...
        dataObj.dataFolderName, ' folder'];
    fid = fopen(fullfile(storPath,folderName,[doNotMove,'.txt']),'w');
    fprintf(fid,doNotMove); fclose(fid);
end %for

end