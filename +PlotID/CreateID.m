function [ID] = CreateID(method)
% CreateID Creates an identifier (char)
%   Creates an (sometimes unique) identifier based on the selected method
%   if no method is selected method 1 will be the default method
arguments
    method (1,1) {mustBeNumeric} = 1
end

switch method 
    case 1 % UNIX Time in seconds as HEX
        ID = posixtime(datetime('now')); %get current Unix time
        ID = dec2hex(int32(ID)); % get it as Hex
        pause(1); %Pausing for unique IDs
    case 2 % random UUID from Java 128 bit
        %Static factory to retrieve a type 4 (pseudo randomly generated) UUID.
        % The UUID is generated using a cryptographically strong pseudo
        % random number generator.
        temp =  java.util.UUID.randomUUID;
        ID = temp.toString;           
     otherwise
        error('The requested method is not defined yet');
end

end
