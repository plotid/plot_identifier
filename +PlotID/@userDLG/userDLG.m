classdef userDLG
    % userDLG the userDLG Class is responsible for the user dialog
    %   error handeling, user messages and warnings will be provided from
    %   this class, depending on the user options and error type.
    %   This reduces messaging overhead and code length in PlotID.Publish()
    
    properties
        configError = false % error state of reading the config
        scriptPublished = false % error state of publishing the script
        rdFilesPublished = false  % error state of publishing the research data
        figurePublished = false  % error state of publishing the figure
        msg = ''        % message to the user
    end
    
    properties (SetAccess = protected)        
        success = false % product of all states
        showMessages {mustBeNumericOrLogical}
        forcePublish {mustBeNumericOrLogical}
        ID %plotID
    end
        
    methods
        function obj = userDLG(ID,options)
            %CATCHERROR Construct an instance of this class
            if isempty(ID)
                ID = '';
            end
            obj.ID = ID;
            obj.showMessages = options.ShowMessages;
            obj.forcePublish = options.ForcePublish;
        end
        
        function output = get.success(obj)
            %   checks if all publish operations are true
            obj.success = all([obj.scriptPublished; obj.rdFilesPublished;...
                obj.figurePublished]);
            output = obj.success;
        end
        
        function obj = set.msg(obj,message)
            %setmsg sets a message text
            obj.msg = message;
        end
        
       function [status] = userDialog(obj,dialogMessage, errorMessage)
            %userDialog displays Y/N user input
            %   if the user selects no an Error will be thrown 
            m = '';
            while ~ismember(m,{'Y','y','n','N'})
              m = input([dialogMessage,', Y/N [Y]? '],'s');
            end
              if ismember(m,{'Y','y'})
                  status = true;
              else
                 obj.error(errorMessage) 
              end           
       end
           
       function [] = userMSG(obj,message)
            %userMSG user message without priority
            %   MSG will only be displaye if ShowMessages is true
            if obj.showMessages
                disp(message);
            end
       end
       
       function [] = error(obj,message)
            %error handles critical errors
            %   critical errors will always result in a matlab error 
            
            %ToDO: Add function(s) before termination
            error(message);
       end 
        
       function [] = softError(obj,message)
            %softError handles soft errors
            %   soft errors can be changed to warning if ForcePublish is
            %   selected by the user
            if obj.forcePublish
                warning(message);
            else
                error(message);
            end
        end
    end
    
    methods(Static)
       function [status] = userQuestion(dialogMessage)
        %userQuestion displays Y/N user input
          m = '';
          while ~ismember(m,{'Y','y','n','N'})
              m = input([dialogMessage,', Y/N [Y]? '],'s');
          end
          if ismember(m,{'Y','y'})
              status = true;
          else
             status = false;
          end           
       end 
    end %static 
    
end % class

