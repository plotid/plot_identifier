function [storagePaths, status, msg] = createFileCopy(filePaths,folderName,storPath,ID,type)
% createFileCopy Creates a copy of the files (can be used for multiple
% paths in a cell array) 
%   
%   folderName is the name of the exporting folder
%   returns the storage paths were files were stored
%   type switches the mode depending on the data type
    
if ~iscell(filePaths)
    %fixes Issue if filepath is a char and not a cell array
    filePaths = {filePaths};
end
        
%try 
    storagePaths = cell(numel(filePaths,1));
    for i = 1:numel(filePaths)
        FileNameAndLocation = filePaths{i};
        [~,name,ext] = fileparts(filePaths{i}); % get the extension

        switch type
            case 'data'                    
                  newfile = sprintf([name,ext]); %keep original name
                  %old behaviour
                  %sufix = '_data';
                  %newfile = sprintf([ID, sufix, '_' , num2str(i) ,ext]);
            case 'dataCentral'
                %keep original name
                newfile = sprintf([name,ext]);
            case 'script'
                sufix = '_script';
                newfile = sprintf([ID, sufix ,ext]);
                %script filename in matlab cannot include '-'
                newfile = regexprep(newfile,'-','_'); 
            case 'userFcn'
                %keep original name
                newfile = sprintf([name,ext]);
            case {'class','toolbox'}
                newfile = name; % copy whole folder
            otherwise 
                error([type,' is not a valid type for createFileCopy'])
        end %switch 

        RemotePath = fullfile(storPath,folderName, newfile);

%           Check if remote file already exists
        
        if isfile(RemotePath) && ismember(type,{'data','dataCentral'})
            % Add a Sufix number to new file name                     
            [~,name,ext] = fileparts(RemotePath); 

            % User Dialog
            msg = ['Filename ',name, ' already exists in the data folder' newline,...
            ' PlotID will add an suffix if you continue.' newline,...
            ' This can cause serious confusions.'];
            warning(msg);                
            m = input('Do you want to continue, Y/N [Y]:','s');

            if ismember(m,{'N','n'})
               errorMSG = ['Filename already exists in data folder.' newline,...
                   ' Rename the File and restart PlotID.'];
               error(errorMSG);
            end 
            % add sufix
            RemotePath = fullfile(storPath,folderName,...
                [name,'_1',ext]);
            
             % auto count until a sufix is reached that did not
                    % exist. (User is only asked once)
            count = 0;
            while isfile(RemotePath)
                count = count + 1;  
                [~,name,~] = fileparts(RemotePath);
                RemotePath = fullfile(storPath,folderName,...
                [name(1:end-length(num2str(count-1))),num2str(count),ext]);
            end 
        end%if         
        copyfile(FileNameAndLocation,RemotePath);
        storagePaths{i} = RemotePath; 
        
        % Comment out call to Publish in Scriptfile 
        switch type
            case 'script'
                scripttext = regexp(fileread(RemotePath),'\n','split');
                commentlines = find(contains(scripttext,'PlotID.'));
                scriptfid= fopen(RemotePath,'w');
                for j=1:length(scripttext)
                    if ismember(j,commentlines)
                        fprintf(scriptfid,'%s %s\n','%',scripttext{j});
                    else
                        fprintf(scriptfid,'%s\n',scripttext{j});
                    end
                end
                fclose(scriptfid);
        end

    end %for
    status = true;
    

    msg =([type, ' successfully published']);
% catch
%     status = false;
%     warning([type,' export was not successful'])
%     if exist('errorMSG')
%         error(errorMSG);
%     end 
% end %try
end %function

