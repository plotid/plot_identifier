function [options] = plotID_options(input)
%PLOTID_OPTIONS gives you a struct with all the options defined in publish
%   Two modes are implemented default and debug

options = struct();
switch input
    case 'default' %same as Arguments Block       
        options.Location  = 'local'; % storage path
        options.Method  = 'individual';
        options.ParentFolder  = 'export';
        options.ConfigFileName = 'config.json';%individual config names possible
        options.CopyUserFCN = true;
        options.CSV = false;
        options.ShowMessages = true;
        options.ForcePublish  = false; %publish anyway
    case 'debug'
        options.Location  = 'local'; % storage path
        options.Method  = 'individual';
        options.ParentFolder  = 'export';
        options.ConfigFileName = 'config.json';%individual config names possible
        options.CopyUserFCN = true;
        options.CSV = true;
        options.ShowMessages = true;
        options.ForcePublish  = true; %publish anyway        
end

end

