classdef config < handle
    % CONFIG class handles methods and attributes related to the config
    %file
    %   handle class used for dynamicy property updates
      
    properties (SetAccess = private)
        mandatoryFields = {'Author', 'ProjectID'} 
        optionFields
        configPath
    end
    
    properties (SetAccess = protected)
        configData
        configFileName
        exportPath = ''
    end
    
    methods
        function obj = config(configFileName)
            %CONFIG Construct an instance of this class
            %   reads config data from config file, if an error occurs the
            %   wizard is started by the catch block
            obj.configFileName = configFileName;
            try %Validity Check
              tmp = what("+PlotID");
              tmp = strrep(tmp.path,'+PlotID','');
              defaultConfigPath = fullfile(tmp,obj.configFileName);
              if isfile(defaultConfigPath)
                 txt = fileread(defaultConfigPath);
              else %search on path
                 txt = fileread(obj.configFileName);
              end

              obj.configData = jsondecode(txt);
              assert(checkConfig(obj));        
              if isfield(obj.configData,'ExportPath')
                  obj.exportPath = obj.configData.ExportPath;
                  obj.configData.options.Location = 'exportPath';
              end
            catch 
                msg = ['no valid config File with the filename ',...
                    obj.configFileName, ' found.' newline,...
                    'Please enter the required information manually'];
                disp(msg); 
                obj.configData = obj.wizard('initialise');
                m = input('Do you want to save the config, Y/N [Y]:','s');
                if ismember(m,{'Y','y'})
                   obj.writeConfig(fullfile(pwd));
                end
            end%try
            obj.configPath = which(obj.configFileName);
        end
        
        function outputArg = checkConfig(obj)
            %checkConfig validates the config file
            % 1. check if mandatory fields are set
            check = isfield(obj.configData,obj.mandatoryFields);
            outputArg = all(check);
            
        end
        
        function writeConfig(obj,path)
            %writeConfig writes the config file to path
            %   TODo;
            fid = fopen(fullfile(path,obj.configFileName),'w');
            txt = jsonencode(obj.configData,'PrettyPrint',true);
            %fprintf does not write paths correctly !!!
            fwrite(fid,txt);
            fclose(fid);
        end
        
        function  configData = addPublishOptions(obj,mode)
            %addPublishOptions adds the publsih options to the config
            %object depending on the mode          
           obj.configData.options = obj.plotID_options(mode);
           cpath = fileparts(obj.configPath); 
           obj.writeConfig(cpath);
        end                 

    end
    
    methods (Static)
        configStruct = wizard(mode)
        optionStruct = plotID_options(input)       

    end
end

