function config = wizard(mode)
%wizard creates config files depending on the selected mode
% initilise ask only for the input that is neccessary to run plotID

config = struct();
switch mode
    case 'initialise'
        config.Author = inputRequired('your Name');
        config.ProjectID = inputRequired('your Project Number');

        msg = ['Do you want to add an export path?' newline,...
            'Otherwise your files will be stored locally.' newline,...
            'You can add this later by rerunning the initiliasation.'];
        disp(msg);                
        m = input('Do you want to add an export path? Y/N [Y]:','s');

        if ismember(m,{'Y','y'})
           config.ExportPath = uigetdir();
        end
    otherwise
        error('wizard mode undefined in CLASS config');
end %switch

end

function usrTxt = inputRequired(msg)
%Input Dialog that repeats if left empty
inputRequired = true;
while inputRequired
    prompt = ['Please enter ', msg , ': '];
    inpt = input(prompt,'s');
    if isempty(inpt)
        disp('Input must not be empty!');
    else
        usrTxt = inpt;
        inputRequired = false;
    end
end
end 